//Código em C, mostrando as formas de formatação  output.

#include <stdio.h>

int main(int argc, char const *argv[])
{
	int intVar = 9999999;
	int intVar2 = 10;
	double doubleVar = 100.123456789;

	/*Quando queremos mostrar o %d, %i, %f, etc, adicionamos um % a mais
	*dessa */

	//Formatação de variáveis inteiras

	//Para números decimais %d ou %i

	/*Podemos formatar também onde irá ser impresso o valor, colocando 
	*números entre o "%" e o tipo de valor "d", "i", etc. O exemplo 
	*abaixo mostra com o número 7 "%7d", fazendo isso, estamos colocando
	*espaços e formatando o texto de saída na tela. 
	*Também é possível escolher quantos números depois da vírgula queremos
	*imprimir, para fazer isso é colocado um ponto seguido do número, entre 
	*o % e o tipo de valor de saída, "%.2f", nesse exemplo, escolhemos duas 
	*casas decimais após a vírgula.*/
	printf("Variável inteira (%%d) = %7d\n", intVar2);
	printf("Variável inteira (%%i) = %i\n", intVar);

	//Para números hexadecimais %x
	printf("Variável inteira (%%x) = %x\n", intVar);

	//Para números octais
	printf("Variável inteira (%%o) = %o\n\n", intVar);





	//Formatação de variáveis float e double
	printf("Variável double (%%f) = %.2f\n", doubleVar);

	//Números em notação científica. 
	printf("Variável double (%%e) = %e\n", doubleVar);

	//Perde a precisão, porém não arredonda os números.
	printf("Variável double (%%g) = %x\n", doubleVar);

	//Também em notação científica, porém com vírgula.
	printf("Variável double (%%a) = %o\n", doubleVar);

	return 0;
}