#include <stdio.h>

int main(){

    //Primeiro programa em C, famoso Hello World.

    /*Outro comentario de outra forma
    so pra lembrar como usar eles*/
    printf("Hello World!\n");

    return 0;
    
    /*Sempre lembrar que no windows ao final do código
    *deve ser incluído o 
    System("pause");
    *porque o código fecha após o final da execução e 
    não é possível ver sua saída sem essa linha de código.*/

}
