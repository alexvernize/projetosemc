//Exemplo de código em C utilizando vetores com estruturas.

#include <stdio.h>

int main(int argc, char const *argv[])
{
	struct horario
	{
		int horas;
		int minutos;
		int segundos;
	};

	//Criação do vetor horaDoBrasil[5] de tipo struct horário.
	//struct horario horaDoBrasil[5];
	/*Basicamente essa struct irá conter 5 vezes o valor do struct horário.
	* No caso 5 vezes o int horas, int minutos e int segundos. Da mesma forma
	* poderia ser criada uma matriz struct horario horaDoBrasil[5][5];*/

	//Para manipular essa struct com vetor usamos a seguinte sintaxe.
	/*
	horaDoBrasil[0].horas = 10;
	horaDoBrasil[0].minutos = 20;
	horaDoBrasil[0].segundos = 30;
	horaDoBrasil[1].horas = 18;
	horaDoBrasil[1].minutos = 35;
	horaDoBrasil[1].segundos = 12;*/

	//Outra forma de inicializar os membros do vetor é a seguinte:
	struct horario horaDoBrasil[5] = {{10, 20, 30}, {12, 15, 43}, {18, 22, 33},
										{35, 15, 14}, {11, 35, 59}};

	int i;
	for (int i = 0; i < 5; ++i)
	{
		printf("%i:%i:%i\n", horaDoBrasil[i].horas, horaDoBrasil[i].minutos, horaDoBrasil[i].segundos);
	}


	//printf("%i:%i:%i\n%i:%i:%i\n", horaDoBrasil[0].horas, horaDoBrasil[0].minutos, horaDoBrasil[0].segundos, 
	//							 horaDoBrasil[1].horas, horaDoBrasil[1].minutos, horaDoBrasil[1].segundos);

	return 0;
}
