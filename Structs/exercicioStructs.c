/*Crie um programa que possui uma função que recebe como argumento 
* um vetor de tamanho 5 de tipo estrutura sendo que essa estrutura 
* deve armazenar um determinado horário no formato hh:mm:ss, peça 
* que o usuário digite 5 horários diversos que deverão ser armazendos 
* no argumento recebido. Crie uma segunda função que receberá este 
* msmo vetor estrutura mas dessa vez a função deverá apenas ler os 
* valores armazendos no vetor estrtura mostrando uma mensagem apropriada.*/

#include <stdio.h>

struct horario
{
	int hora;
	int minuto;
	int segundo;
};

int main(int argc, char const *argv[])
{
	void receberHorarios(struct horario lista[5]);
	void printfHorarios(struct horario lista[5]);

	struct horario listaHorarios[5];

	receberHorarios(listaHorarios);
	printfHorarios(printfHorarios);

	return 0;
}

void receberHorarios(struct horario lista[5])
{
	int i;
	for (int i = 0; i < 5; ++i)
	{
		printf("Digite o %i horarios(hh:mm:ss): ", i + 1);
		scanf("%d:%d:%d", &lista[i].hora, &lista[i].minuto, &lista[i].segundo);
	}
}

void printfHorarios(struct horario lista[5])
{
	int i;
	for (int i = 0; i < 5; ++i)
	{
		printf("O %i horário é = %i:%i:%i\n", i+1, lista[i].hora, lista[i].minuto, lista[i].segundo);
	}
}
