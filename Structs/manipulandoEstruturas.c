//Exemplo de código em C manipulando estruturas.
#include <stdio.h>


int main()
{
	/*Vetores em C só podem armazenar valores di tipo int. É possível armazenar na 
	*linguagem C outros tipos de valores como char, int, double, usando estruturas.*/

	//Para definir uma estrutura nós utilizamos a seguinte sintaxe.
	struct horario
	{
		int horas;
		int minutos;
		int segundos;
		double numero;
		char letra;
		/*Poderia colocar aqui dentro, um float, um char, um double, etc.
		*int float numero2;*/
	};

	//Para declarar estruturas utilizamos a seguinte sintaxe. 
	struct horario agora;
	//No exemplo acima é criado a estrutura de tipo horário com o nome de agora.

	/*Para alterar os valores de cada uma das variáveis da estrutura, é utilizado o nome da 
	*estrutura "." o nome da varíavel. Um exemplo dessa sintaxe seria em nossa estrutura do 
	*tipo horário "struct horario" com  nome "agora" alterando a variável horas, agora.horas.*/
	agora.horas = 15;
	agora.minutos = 17;
	agora.segundos = 30;

	//Estrutura declarada para manipular a estrutura "agora". Dessa forma é possível
	//realizar expressões matemáticas utilizando estruturas.
	struct horario depois;
	depois.horas = agora.horas + 10;
	depois.minutos = agora.minutos + 10;
	depois.segundos = agora.segundos + 10;
	depois.numero = 50.125/123;
	depois.letra = 'a';

	//Exemplo de printf em estruturas.
	printf("%i:%i:%i\n", agora.horas, agora.segundos, agora.minutos);
	printf("%i:%i:%i\n%c\n%f\n\n", depois.horas, depois.segundos, depois.minutos, depois.letra, depois.numero);


	return 0;
}