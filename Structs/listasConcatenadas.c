//

#include <stdio.h>

int main()
{
	struct lista
	{
		int valor;

		//Ponteiro com nome de 'proximo' que aponta em uma estrutura lista.
		struct lista *proximo;
	};

	struct lista m1, m2, m3;

	//Ponteiro 'gancho' apontando no endereco de memoria de m1
	struct lista *gancho = &m1;

	m1.valor = 10;
	m2.valor = 20;
	m3.valor = 30;

	m1.proximo = &m2;
	m2.proximo = &m3;

	//Como nao temos outro membro, temos que apontar em um valor nulo
	//podemos simplesmente colocar como '0', ou escrever como foi feito
	//abaixo.
	m3.proximo = (struct lista*)0;
	 
	//Laco 'while' criado para dar print nos valores da struct, ele tem seu 
	//final quando a struct tiver o valor de nulo, no ultimo que colocamos.
	while(gancho != (struct lista *)0)
	{
		//printf com os valor mostrados na tela
		printf("%i\n", gancho->valor);
		
		//esse eh o incremento do laco, a cada iteracao o valor de gancho
		//sera o proximo valor do ponteiro que colocamos.
		gancho = gancho->proximo;
	}

	getchar();

	return 0;
}
