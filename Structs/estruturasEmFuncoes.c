//Passar e receber estruturas a uma função.

#include <stdio.h>

//Definimos a estrutura, acima das funções para ela ser global.
struct horario
{
	int horas;
	int minutos;
	int segundos;
};

int main()
{
	//Declaração da função com a estrutura
	/*Essa função é uma struct do tipo horario com o nome de funcaoStruct, e recebe como argumento
	*uma estrutura do tipo horario com o nome de x.*/
	struct horario funcaoStruct(struct horario x);

	//Criação da estrutura de tipo horário com o nome de agora.
	struct horario agora;
	
	agora.horas = 15;
	agora.minutos = 17;
	agora.segundos = 30;

	/*Para chamar a função com a struct usamos a seguinte sintaxe.
	funcaoStruct(agora)*/

	struct horario proxima;
	proxima = funcaoStruct(agora);

	//Exemplo de printf em estruturas.
	printf("%i:%i:%i\n", proxima.horas, proxima.segundos, proxima.minutos);

	return 0;
}

struct horario funcaoStruct(struct horario x)
{
	printf("%i:%i:%i\n", x.horas, x.minutos, x.segundos);

	x.horas = 100;
	x.minutos = 100;
	x.segundos = 100;

	return x;
};