//Exemplo de código em C inicializando estruturas.

#include <stdio.h>

int main(void)
{	
	struct horario
	{
	int horas;
	int minutos;
	int segundos;
	}agora, depois, amanha;

	//É criar a estrutura já ao final da declaração da mesma.
	/*No exemplo acima após a criação, ao final das chaves, 
	 * foram criados nomes para as estruturas. Dessa forma 
	 * não seria necessário colocar a linha de código abaixo.*/

	//struct horario agora;
	/*Também é possível criar, declarar e inicializar ao mesmo
	 * tempo, com a seguinte sintaxe:
	 *
	 * struct horario
	 * {
	 * int horas;
	 * int minutos;
	 * int segundos;
	 * }agora  = {10, 20, 30}
	 *
	 * dessa forma as linhas abaixo não seriam necessárias.
	 * Nessa inicialização, os valores seguem a ordem, entáo 
	 * 10 seria a variável horas, 20 a variável minutos e 30 a 
	 * variável segundos.*/

	/*Outra forma seria: 
	 * struct horario agora = {10, 20,30};
	 * dessa forma ele estaria na sequência também, para escolher 
	 * quais as variáveis pelo nome, utilize o formato abaixo:
	 * struct horario agora = {.segundos = 30, .horas = 10, .minutos = 20};*/

	agora.horas = 18;
	agora.minutos = 45;
	agora.segundos = 33;
	

	printf("A hora agora é: %i:%i:%i\n", agora.horas, agora.minutos, agora.segundos);

	return 0;
}
