//Exemplo de Casting em C

#include <stdio.h>

int main(){
	//Declaração das variáveis.
	int x = 16;
	int y = 3;
	
	/*Como as variáveis x e y são do tipo int e a resultado do tipo float
	*o programa não consegue entender e acaba traduzindo como se o resultado
	*fosse do tipo int. Para obter um resultado com casas decimais, devemos
	*realizar o casting nas variáveis inteiras.*/

	//Para realizar o casting colocamos o tipo de variável que queremos entre
	//parênteses antes das variáveis que desejamos mudar. No exemplo colocamos
	//(float)x/y e o resultado será uma variável do tipo float. 
	float resultado = (float)x/y;
	
	//O casting funciona para qualquer tipo de variável, pode ser do tipo char,
	//double, float, int, etc.

	printf("%f\n", resultado);

	return 0;
}
