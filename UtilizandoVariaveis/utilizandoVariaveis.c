//Utilizando variáveis

#include <stdio.h>

int main (){

    //Algumas formas de colocar valores em variáveis.

    //Colocar o tipo de variável e dar apenas o nome para depois colocar seu valor.
    int minhaIdade;
    minhaIdade = 23;

    //colocar o tipo, nome e valor simultaneamente
    int maeIdade = 48;
    int paiIdade = 49;

    //Barra invertida n "\n" serve para pular linhas
    printf("Minha idade = %i\nPai idade = %i\nMae Idade = %i\n", minhaIdade, paiIdade, maeIdade);

    return 0;
}
