//Biblioteca Locale utilizado para acentua��o.
#include <stdio.h>

//Inclus�o da biblioteca
#include <locale.h>

int main(){

    //Setamos o idioma que queremos utilizar na acentua��o, no caso portugu�s.
    setlocale(LC_ALL, "Portuguese");

    printf("Ol� mundo\nTestando acentua��o\n");

    return 0;

}
