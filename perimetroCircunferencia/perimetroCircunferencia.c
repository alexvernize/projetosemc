#include <stdio.h>

//Biblioteca Locale utilizada para acentua��o.
#include <locale.h>

#include <math.h>
#include <stdlib.h>

int main(){

    //Setamos o idioma que queremos utilizar na acentua��o
    setlocale(LC_ALL, "Portuguese");

    //cria��o das vari�veis para c�lculo de per�metro
    float r, perimetro;
    r = 2,51;
    perimetro = 2*M_PI*r;
    printf("O resultado do per�metro da circunfer�ncia �: %f \n\n", perimetro/*Para vari�veis com decimal, utilizamos
           o float "f" e no comando printf � colocado com o s�mbolo de porcentagem "%f", ao final das aspas do texto
           colocamos uma v�rgula e o nome da vari�vel que queremos mostrar*/);

    printf("Ol� mundo\n\nTestando acentua��o\n");

    return 0;

}
