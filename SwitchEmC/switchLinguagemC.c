//Exemplo do comando switch em linguagem C

#include <stdio.h>

int main(){

	int i;
	printf("Insira um número inteiro de 1 a 5: ");
	scanf("%i", &i);
	/*Entre os parênteses é colocada a variável que queremos analisar, 
	 * ela será a que o usuário escolher. Após isso o comando do valor selecionado
	 * será executado.*/
	switch (i){
		
		case 1:
			printf("primeiro\n\n");
			/*Não esquecer de colocar o comando break a cada
			case, ele que irá finalizar o switch, pulando do
			valor selecionado para o final do switch. Ele avisa 
			o software que foi encontrada a variável que buscávamos.
			Se não fosse colocado o break ele iria rodar cada uma das linhas do switch
			que não possuírem o break.*/
			break;
		case 2:
			printf("segundo\n\n");
			break;
		case 3:
			printf("terceiro\n\n");
			break;
		case 4:
			printf("quarto\n\n");
			break;
		case 5:
			printf("quinto\n\n");
			break;
		default:
			printf("Opção inválida!\n\n");
			break;
	}

	return 0;
}
