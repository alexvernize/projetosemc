#include <stdio.h>

int main(void)
{
	int x = 10;
	double y = 20.50;
	char z = 'a';
	
	//Existem duas formas de declarar e inicializar um ponteiro em C.
	//Uma é primeiro declarar e depois inicializar. Outra forma é na mesma
	//linha inicializar e declarar. A diferença é que da primeira forma, 
	//quando declaramos a variável, utilizamos o '*' e na segunda, utilizando
	//a mesma linha só colocamos uma vez o nome da variável com o '*'.
	//int *ponteiroX;
	//ponteiroX = &x;
	
	int *ponteiroX = &x;
	double *ponteiroY = &y;
	char *ponteiroZ = &z;

	double soma = *ponteiroX + *ponteiroY;

	int *resultado;
	resultado = &x; 

	printf("Endereço x = %i e Valor x = %i\n", ponteiroX, *ponteiroX);
	printf("Endereço x = %i e Valor x = %f\n", ponteiroY, *ponteiroY);
	printf("Endereço x = %i e Valor x = %c\n", ponteiroZ, *ponteiroZ);
	printf("A soma do ponteiroX com o ponteiroY é: %f\n", soma);
	printf("Valor de x = %i\n", *resultado);
	getchar();

	return 0;
}
