#include <stdio.h>

int main(void)
{
	/* Só pra lembrar como criar um ponteiro que aponta pra uma variável 
	 * inteira
	 * Primeiro definimos a variável, nesse caso uma int de nome x com valor
	 * 10. Depois criamos o ponteiro, primeiro dizemos o tipo de dado do 
	 * ponteiro, que nesse caso é uma int após um asterísco que simboliza
	 * que isso é um ponteiro, seguido de um nome e por último informo o 
	 * endereço de memória onde o ponteiro está apontando, no caso em x, 
	 * para isso usamos o sinal de e comercial '&'.
	 * 
	 * int x = 10;
	 * int *ponteiro = &x;
	*/
	
	//Para apontar para um vetor, duas coisas são diferentes da declaração 
	//de ponteiros, primeiro que não é necessário no ponteiro declarar o 
	//tamanho do vetor ex: *ponteiro[3], segundo, não é necessário apontar 
	//utilizando o e comercial '&', o compilador automaticamente irá apontar
	//no primeiro membro do vetor.
	int vetor[3] = {1, 2, 3};
	int *ponteiro = vetor;
	
	//Para forçar pro ponteiro apontar ao valor que você deseja no vetor é 
	//só adicionar entre colchetes a posição que deseja, ex: 
	//int *ponteiro[3] = &vetor[1], repare que dessa forma é necessário 
	//colocar o e comercial '&'.
	int *ponteiro2 = &vetor[1];
	
	//Se ao invés de ver o valor do ponteiro eu quiser ver o endereço de memória
	//você deve retirar o asterísco e colocar apenas o nome do ponteiro, a maneira 
	//correta para ler o endereço de memória é com o '%p'.
	printf("%p\n", ponteiro); 
			
	printf("%i\n", *ponteiro);
	printf("%i", *&vetor[1]);

	getchar();
	return 0;
}
