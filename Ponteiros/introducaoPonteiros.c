//Software de introdução a ponteiros na linguagem C


#include <stdio.h>

int main (void)
{
	//Declarando uma variável x
	int x;
	
	//Inicializando uma variável x, esse é o valor da variável,
	//em ponteiros nós acessamos o endereço dessa variável na 
	//memória, onde a variável está localizada.
	x = 10;
	
	//Para criar um ponteiro, primeiro declaramos o tipo seu tipo,
	//nesse exemplo utilizamos um int e após colocamos  o carater '*'
	//seguido pelo nome do ponteiro que será criado. Após isso, devemos
	//apontar para o espaço da memória que queremos que o ponteiro utilize,
	//isso é criado com o nome do ponteiro seguido do sinal igual e o 
	//caracter especial '&' mais o nome da variável que queremos que ele 
	//utilize o valor. 
	int *ponteiroTeste;	
	ponteiroTeste = &x;

	//Para acessar o endereço onde a variável está localizada, utilizamos
	//o caracter especial '&' seguido do nome da variável.
	printf("%i\n", &x);

	//Utilizando o '*' printamos o valor da variável x
	printf("%i\n", *ponteiroTeste);

	//Sem utilizar o '*' printamos o endereço da variável x 
	printf("%i\n", ponteiroTeste);

	//getchar();
	return 0;
}
