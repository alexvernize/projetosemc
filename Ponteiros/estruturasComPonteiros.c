#include <stdio.h>

int main()
{
	/*
	 * Criação da estrutura com nome horario, com elementos do 
	 * tipo inteiro hora, minuto e segundo. 
	 * */
	struct horario
	{
		int hora;
		int minuto;
		int segundo;
	};
	
	/*
	 * Declaração de uma estrutura do tipo horario chamada agora
	 * com um ponteiro que está apontando para a estrutura do tipo
	 * horario com o nome de depois (Repare que o ponteiro é escrito
	 * após uma vírgula).
	 * */
	struct horario agora, *depois;

	/*
	 * Para fazer o ponteiro '*depois' apontar para a estrutura agora,
	 * utilizamos a mesma sintaxe utilizada para ponteiros, como demons-
	 * trado abaixo. 
	 * */
	depois = &agora;
	
	/*
	 * A sintaxe para alterar os valores da estrutura usando ponteiro 
	 * é um pouco diferente da usada em estruturas sem utilizar ponteiros.
	 * O compilador entende como erro a seguinte sintaxe '*depois.hora = 20;',
	 * ele sempre age parecido com a lógica matemática, ao qual o '.' sempre 
	 * vem antes de outra coisa, uma das maneiras de conseguir acessar um 
	 * ponteiro de tipo estrutura é colocar o '*depois' entre parênteses.
	 * */
	//(*depois).hora = 20;
	//(*depois).minuto = 33;
	//(*depois).segundo = 54;

	/*
	 * Outra sintaxe utilizada em ponteiros com estruturas é utilizando
	 * '->' no lugar do ponto. Esta é a sintaxe mais usada entre programadores.
	 * */

	depois -> hora = 20;
	depois->minuto = 36;
	depois ->segundo = 54;

	printf("%i:%i:%i\n", agora.hora, agora.minuto, agora.segundo);

	return 0;
}
