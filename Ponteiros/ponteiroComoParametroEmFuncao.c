#include <stdio.h>

int main(void)
{
	//Declaracao de duas funcoes
	void testeVariavel(int x);
	void testePonteiro(int *pX);

	//variavel teste com valor de 1
	int teste = 1;
	
	//criacao de um ponteiro apontando para a variavel teste
	int *pTeste = &teste;
	
	//Chamamos a funcao e essa funcao adiciona 1 ao valor da variavel 
	//teste, que no momento eh o valor de 1 tambem.
	testeVariavel(teste);
	
	//Chamamos a funcao que adiciona um ao valor de memoria apontado
	//quando passamos um valor de ponteiro pra uma funcao, nao colocamos
	//o '*', repare abaixo a sintaxe. Porque nesse caso queremos passar
	//o endereco da memoria e se usassemos o '*', estariamos passando 
	//o valor que o ponteiro esta apontando.
	testePonteiro(pTeste);

	//Aqui printamos a variavel teste
	printf("%i\n", teste);

	getchar();
	return 0;
}

//Criacao de uma funcao que recebe como argumento uma variavel inteira
//a funcao apenas adiciona 1 a variavel x
void testeVariavel(int x)
{
	++x;
	printf("%i\n", x);
}

//Criacao de uma funcao que recebe um endereco de memoria (recebe um ponteiro)
//a funcao adiciona 1 ao valor que esse ponteiro aponta.
void testePonteiro(int *pX)
{
	++*pX;
}
