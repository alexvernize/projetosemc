/*Exemplo de como utilizar n�meros octais e n�meros hexadecimais Tamb�m mostra a
diferen�a do uso de %d e %i, em output n�o existe diferen�a alguma do uso de um
ou outro, mas o mesmo n�o acontece tratando-se de fun��es de input, por exemplo scanf().
Utilizar %d implica que o valor digitado pelo usu�rio ser� interpretado como um
n�mero inteiro decimal. Se o software receber dados de input com valores octais ou hexadecimais
deve ser utilizado o %i.
*/

#include <stdio.h>

int main(){

    int a = 1000; // n�mero 1000 escrito na base de 10(decimal)

    //Repare que � adicionado o 0 na frente do n�mero para reconhecer o valor como octal.
    int b = 01750; //n�mero 1000 escrito na base de 8(octal)

    //Repare que � adicionado o 0x na frente do n�mero para reconhecer o valor como hexadecimal.
    int c = 0x3E8; //n�mero 1000 escrito na base de 16(hexadecimal)

    //Utilizar %i ou %d para output, n�o tem diferen�a, por�m input utilize sempre o %i.

    printf("%i\n", a);

    printf("%i\n", b);

    printf("%i\n", c);

    printf("\n");

    printf("%d\n", a);

    printf("%d\n", b);

    printf("%d\n", c);

        return 0;
}
