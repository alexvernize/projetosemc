//Exemplo de ciclo while em C

#include <stdio.h>

int main (){
	
	int contador = 1;
	/*Apesar de ter a mesma função do ciclo for, o ciclo while 
	 * tem algumas diferenças em sua sintaxe. No ciclo for colocamos
	 * basicamente tudo o que queremos entre os parênteses, já o 
	 * ciclo while colocamos apenas a condição entre os parênteses. 
	 * */
	while (contador <= 5){
		
		printf("%i\n", contador);
		//Aqui dentro das chaves fazemos o incremento do contador
		//com o ++ indicamos que adicionamos ele e mais um em cada 
		//ciclo while que rodarmos. Se esquecermos do contador teremos
		//um loopintão (:
		++contador;

	}

	return 0;
}
