# Programas em C

### Alguns programas em C básicos para aprendizado da linguagem de programação

![](imagens/linguagem-c.png)

Nosso querido Hello World em C

```c

#include <stdio.h>

int main(){
    
    printf("Hello World!");
    
    return 0;
}

```