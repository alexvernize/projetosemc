//Exemplo de código em C com Strings.
/*Existem duas formas de usar strings, uma delas é utilizar a biblioteca
 * "#include <string.h>", que é uma biblioteca padrão da linguagem C. 
 * A outra forma é utilizando vetores, dessa forma criaríamos um conjunto
 * de caracteres "char letras[];"*/

#include <stdio.h>


int main(void)
{
	//String é uma cadeia de caracteres: char letra = 'a';
	/*Uma forma de utilizar strings é através de vetores utilizando
	 * variáveis do tipo char. a sintaxe é expressa abaixo.*/
	char teste[] = {'O', 'l', 'a', ' ', 'M', 'u', 'n', 'd', 'o', '!'};

	int i;

	for (i = 0; i < 10; ++i)
	{
		printf("%c", teste[i]);
	}
	printf("\n");

	return 0;
}
