//Código em C para exemplificar o operador condicional "?"


#include <stdio.h>

int main(){

	int num1 , num2;
	printf("O primeiro número é maior que o segundo?\n");
	printf("Digite o primeiro numero: ");
	scanf("%i", &num1);
	
	printf("Digite o segundo numero: ");
	scanf("%i", &num2);
	
	/*O uso do operador "?" funciona da seguinte forma, primeiro colocamos a condição,
	 * depois adicionamos o operador e em seguida as ações desejadas que ele faça. 
	 * Entre as ações é utilizado os dois pontos ":".*/
	num1 > num2 ? printf("sim\n")  : printf("nao\n");	

	return 0;
}
