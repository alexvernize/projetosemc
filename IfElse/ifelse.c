//Exemplo de código de tomadas de decisão If Else em linguagem C.

#include <stdio.h>

int main (){
	
	int idade;

	printf("Favor informa a sua idade: ");
	scanf("%i", &idade);
	/*Para utilizar a tomada de decisão if, é criada uma condição
	 * dentro do parênteses, se a condição atender aos requisitos
	 * os comandos dentro das chaves serão executados.  */
	if(idade >= 18 && idade <= 25){
	
		printf("\nVocê é VIP tem direito a openbar :) \n\n");
	
	}else if(idade > 25){
		
		printf("\nVocê tem idade para beber :) \n\n");
	//Pode ser utilizado chave para colocar o printf dentro
	//ou também apenas colocar abaixo sem chaves do if, elseif e else.
	}else if(idade < 18 && idade >= 15)
		printf("\nVocê não tem idade para beber, se fode aí :( \n\n");
	
	else if(idade < 15 && idade >= 10)
		printf("\nSeus pais sabem que quer beber? :D \n\n");
	else
		printf("\nMas você é uma criança :O \n\n");

	return 0;
}
