//Variáveis float, int, char
//Básico em C, nunca esquecer o ;
#include <stdio.h>

//Variáveis tipo bool, é necessário adicionar a biblioteca stdbool.h
#include <stdbool.h>

int main (){

	//variável do tipo int imprimimos usando o %i
	int inteira = 10;
	printf("\nCom o int os valores são sempre inteiros: %i\n\n", inteira);

	/*variável do tipo long int imprimimos usando o %li
	*É utilizada quando usamos números de grandeza maior que o int
	*Utiliza mais recursos de memória, então se não for necessário
	*utilizar o int.*/
	long int variavelLongInt = 87559845562123548; 
	printf("Com o long é possível utilizar números muito grandes: %li\n\n", variavelLongInt); 

	/*Variável do tipo const, pode ser utilizada com qualquer tipo
	 * de variável, ela será uma constante que não poderá ser alterada 
	 * depois de declarada.*/
	const float pi = 3.1415926;
	printf("Com o const criamos constantes para o código como o valor de pi: %f\n\n", pi);

	/*Também é possível utilizar variáveis com valores somente positivos
	 * com o unsigned, que funciona em qualquer tipo de variável também , 
	 * como o const.*/
	unsigned int positivo = 100;
	printf("Com o unsigned o valor só pode ser positivo %i\n\n", positivo);

	//variável do tipo float imprimimos usando o %f
	float decimal = 10.5;
	printf("Para números decimais utilizamos o float: %f\n\n", decimal);

	/*Variável do tipo double imprimimos usando o %f, ela possui uma precisão
	*maior que a variável do tipo float.*/
	double variavelDouble = 10.643327632764725183132136743;
	printf("A variável double é utilizada para decimais muito grandes: %f\n\n", variavelDouble);
	/*Para pegar valores do usuário com o scanf, NÃO É UTILIZADO O %f, no double
	*é utilizado o %lf.*/
	
	//Variável do tipo long double imprimimos com o %Lf.
	long double variavelLongDouble = 1025.12445;
	printf("Mais um exemplo de long agora com o double: %Lf\n\n", variavelLongDouble);

	//variável do tipo char imprimimos usando o %c
	char caractere = 'a';
	printf("Podemos imprimir caracteres utilizando o char: %c\n\n", caractere);

	//Variável do tipo bool (true ou false, no caso 0 ou 1), imprimimos usando o %i
	bool boolean = true;
	printf("Esse é o tipo de variável bool: %i\n\n", boolean);

	return 0;
}
