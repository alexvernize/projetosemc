//Exemplo de código declarando vetores.

#include <stdio.h>

int main(){
	
	//Para atribuir valores para vetores utilizamos as chaves.
	int vetor [5] = {20, 10, 15, 25, 5};

	for(int i = 0; i < 5; ++i){
		printf("%i\n", vetor[i]);
	}

	return 0;
}
