//Introdução a vetores com um exemplo de código em C.

#include <stdio.h>

int main(){
	
	/*Vetores são usados para guardar diversos valores 
	* do mesmo tipo em um única variável. Vetores são 
	* utilizados da seguinte forma: int a[];, colocamos 
	* o tipo e o nome da variável e em seguida colocamos 
	* um colchetes. */
	float nota[5];//esse exemplo nota pode armazenar 5 valores do tipo float.

	//Importante lembrar, em vetores o início sempre é em zero, então 
	//no exemplo temos um vetor com 5 valores que vão de 0 a 4
	nota[0] = 5.5;
	nota[1] = 8.5;
	nota[2] = 3.2;
	nota[3] = 9.0;
	nota[4] = 7.5;
	
	//Para escolher quantos números depois da casa decimal queremos que 
	//o nosso código tenha, utilizamos o "." + o número de casas que 
	//queremos, aqui coloquei com uma casa depois da vírgula "%.1f".
	printf("%.1f\n", nota[4]);
	
	return 0;
}
