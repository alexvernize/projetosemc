//Exemplo de código em C com funções.

#include <stdio.h>

int main(int argc, char const *argv[])
{
	/*Para usar uma função, primeiro declaramos a função 
	*(Somente é necessário a declaração, se a função for 
	*criada abaixo da função principal, se ela vier acima, 
	*logo após os "includes", não precisamos declarar, porém
	*para manter o código organizado, é sempre bom declarar
	todas as funções criadas.).
	*Em seguida chamamos a função para executar sua ação.*/
	void imprimaMensagem(void);
	imprimaMensagem();

	return 0;
}

/*A função é criada fora da função principal, colocamos 
*primeiramente o tipo que ela é, nesse exemplo usamos o 
tipo "void", pois, nosso função não retorna nada, depois 
*colocamos o nome que queremos para a função, e por último
*abrimos um parênteses com o seu tipo e abrimos as chaves 
*onde iremos criar nossas linhas de código. Aqui apenas como
*como exemplo, criamos um printf com o Hello World.*/
void imprimaMensagem(void)
{
	printf("Hello World!\n");
}