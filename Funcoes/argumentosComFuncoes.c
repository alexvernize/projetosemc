//Exemplo de código em C mostrando o comportamento de argumentos em funções.

#include <stdio.h>

int main(int argc, char const *argv[])
{
	//Declaração da função print.
	void funcaoPrint(int x, int vetor[]);

	//Declaração de uma variável int e um vetor.
	int x = 10;
	int vetor[3] = {10};

	//Função sendo chamada para a execução.
	funcaoPrint(x, vetor);

	//Comandos printf dentro da função principal.
	/**/
	printf("Variavel int na função principal = %i\n", x);
	printf("Vetor na função principal = %i\n\n", vetor[1]);

	

	return 0;
}			

//Função print.
//Essa função não retorna valores, por isso o uso de void.
void funcaoPrint(int x, int vetor[])
{
	//Alteração dos argumentos recebidos na função.
	x = x +10;
	vetor [1] = 20;

	//Comandos printf dentro da funçãoPrint.
	printf("Variavel int na função print = %i\n", x);
	printf("Vetor na função print = %i\n\n", vetor[1]);
} 