//Exemplo de código passando uma matriz como argumento para uma função.

#include <stdio.h>

int main(int argc, char const *argv[])
{
	//Declaração da função
	void FuncaoPrint(int matrizFuncao[3][3]);

	//Declaração de uma matriz 3x3.
	int matriz [3][3] = {1,2,3,4,5,6,7,8,9};

	//Chamando a função para ser executada;
	//Na chamada da função, não é necessário utilizar os colchetes.
	FuncaoPrint(matriz);

	return 0;
}			

//Função responsável por ler a matriz
/*Existem duas formas de passar uma matriz como argumento de uma função.*
*O primeiro "int matrizFuncao[][3]", você pode omitir o primeiro valor da,*
*matriz, a segunda forma é colocando ambos os valores da matriz, como por*
*exemplo "int matrizFuncao[3][3]".*/
void FuncaoPrint(int matrizFuncao[][3])
{
	//Declaração de duas variáveis.
	int i, j;

	//Laço responsável por ler os valores das matrizes.
	for (i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("%i", matrizFuncao [i][j]);
		}
		printf("\n");
	}
} 