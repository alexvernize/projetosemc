//Software em C utilizando funções para calcular a área de um quadrado

#include <stdio.h>

int main(int argc, char const *argv[])
{
	float calcularAreaRetangulo(float x, float y);
	float x, y;
	printf("Digite a base do retângulo: ");
	scanf("%f", &x);
	printf("Digite a altura do retângulo: ");
	scanf("%f", &y);
	float area = calcularAreaRetangulo(x, y);
	printf("A área do retângulo é: %.2f\n\n", area);


	return 0;
}

float calcularAreaRetangulo(float base, float altura)
{
	float area = base*altura;
	return area;
}