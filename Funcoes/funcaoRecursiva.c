//Exemplo de código em C com funções recursivas.
//Uma função recursiva é uma função que chama ela própria.
//Este programa calcula o fatorial de um número.

#include <stdio.h>

int main(void)
{
	//Declarando a função.
	long int fatorial (int x);

	//Declarando as variáveis.
	long int numero, resultado;

	//Pedindo o número para o usuário.
	printf("Digite um número inteiro: ");
	scanf("%li", &numero);

	//Variável resultado recebendo o valor da função que calcula o fatorial.
	resultado = fatorial(numero);

	//O resultado do cálculo.
	printf("O fatorial de %li é: %li\n\n", numero, resultado);

	return 0;
}
long int fatorial (int x)
{
	long int resultado;

	if(x == 0)
	{
		resultado = 1;
	}
	else
	{
		resultado = x*fatorial(x - 1);
	}

	return resultado;
}