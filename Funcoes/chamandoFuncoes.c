#include <stdio.h>
#include <stdlib.h>
 
void exibeBoasVindas()
{
  printf("===== Ola, seja bem vindo ======\n\n");
}
 
float calculaMedia(float nota1, float nota2)
{
  float media = (nota1 + nota2) / 2;
  return media;
}
 
int main()
{
  // Executa a função de boas vindas
  exibeBoasVindas();
 
  float temp;
 
  // Executa a função para calcular a media
  temp = calculaMedia(9,7);
 
  printf("Resulado da funcao media: %.2f \n", temp);
}