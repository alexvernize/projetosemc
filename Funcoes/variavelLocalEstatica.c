/*Exemplo de código em C com a variável local estática
*A diferença da variável local estática para a automática
*é que a estática não perde o valor dela cada vez que a função 
*é chamada, ela não é recriada automaticamente. Ela mantém o valor
*da execução anterior. Lembrando que ambas as variáveis, são vistas 
*apenas pela função onde foram criadas, Não podendo ser usadas na 
*função principal (main).*/

#include <stdio.h>

int main(void)
{
	void variavelLocal(void);

	variavelLocal();
	variavelLocal();
	variavelLocal();
	variavelLocal();

	return 0;
}

void variavelLocal(void)
{
	//Aqui temos a variável local automática.
	int variavelLocalAutomatica = 2;
	variavelLocalAutomatica *= 2;

	/*A diferença da variável local estática para a automática
	*é que a estática não perde o valor dela cada vez que a função 
	*é chamada, ela não é recriada automaticamente. Ela mantém o valor
	*da execução anterior.*/

	/*Para criar uma variável local estática em C, incluímos o static
	*antes de declarar o tipo de variável, no caso do exemplo abaixo
	*"static int".*/

	//Aqui temos uma variável local estática.
	static int variavelLocalEstatica = 2;
	variavelLocalEstatica *= 2;

	printf("Variável local automática: %i\n", variavelLocalAutomatica);
	printf("Variável local estática: %i\n\n", variavelLocalEstatica);
}