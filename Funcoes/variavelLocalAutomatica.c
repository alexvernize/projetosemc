//Exemplo de código em C com variável local automática

#include <stdio.h>

int main(void)
{
	void variavelLocal(void);

	variavelLocal();
	variavelLocal();
	variavelLocal();
	variavelLocal();

	return 0;
}

void variavelLocal(void)
{
	/*Essa variável tem o nome de variável local automática,
	*porque ela é visível apenas para a função variavelLocal, 
	*ou seja, não possível utilizar essa variável na função 
	*main. Quando é criada uma variável dentro de uma função, 
	*ela é 	*visível apenas dentro da mesma. Ela é automática
	*porque a cada vez que ela é chamada ela é recriada e perde
	o valor anterior.*/
	int variavel = 2;
	variavel *= 2;

	printf("%i\n", variavel);
}