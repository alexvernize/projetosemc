//Exemplo de código em C sobre variável global.

/*Variáveis globais são visíveis a todas as funções. Elas 
*podem ser usadas em qualquer das funções existentes no código.*/

#include <stdio.h>

/*Para criar uma variável global, devemos declarar esta variável 
*após os "#include", dessa forma qualquer função que for criada
*poderá utilizar essa variável.*/

/*É uma boa prática colocar o g a frente do nome da variável para 
*facilitar a leitura, dessa forma quem estiver lendo o código poderá
*saber com uma maior facilidade que se trata de uma variável global.
*Variáveis globais se comportam como variáveis estáticas.*/
int gVariavelGlobal = 2;

int main(void)
{
	void variavelLocal(void);
	//Aqui temos uma variável global sendo usada na função principal
	printf("Variável global na função principal: %i\n\n\n", gVariavelGlobal);

	variavelLocal();
	variavelLocal();
	variavelLocal();
	variavelLocal();

	return 0;
}

void variavelLocal(void)
{
	//Aqui temos a variável local automática.
	int variavelLocalAutomatica = 2;
	variavelLocalAutomatica *= 2;

	//Aqui temos uma variável local estática.
	static int variavelLocalEstatica = 2;
	variavelLocalEstatica *= 2;

	printf("Variável global na função variavelLocal: %i\n", gVariavelGlobal);
	printf("Variável local automática: %i\n", variavelLocalAutomatica);
	printf("Variável local estática: %i\n\n", variavelLocalEstatica);
	
}