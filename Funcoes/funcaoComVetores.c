//Exemplo de código em C com funções recebendo vetores como argumentos.
//Programa que ordena os números de um vetor em ordem crescente.

#include <stdio.h>

int main(int argc, char const *argv[])
{
	//Criação do vetor.
	int vetor[10] = {6, 4, 1, 9, 5, 7, 2, 0, 8, 3};

	//Variável criada para ler o vetor.
	int i;

	//Função sendo declarada.
	/*Quando utilizamos vetores unidimensionais, ou seja, com uma linha apenas,  
	*não é necessário colocar quantos valores meu vetor possui, por exemplo 
	*"void ordemCrescente(int vetor [])" e "void ordemCrescente(int vetor[10])"
	*podem ser usadas essas duas formas.*/
	//Dois valores passados para a função, um vetor e uma variável int que irá
	//informar o número de membros que o vetor possui.
	int ordemCrescente(int vetor [], int n);

	//Função sendo chamada para a execução.
	//Na chamada da função, não é necessário colocar o colchetes "[]".
	ordemCrescente(vetor, 10/*Esse é o número de membros do meu vetor*/);

	for(int i =0; i < 10;)
	printf("O vetor em ordem crescente: %i\n", vetor[i]);


	return 0;
}
//Função para ordenar os números.
int ordemCrescente(int vetor[], int n)
{
	//Declaração das variáveis.
	int i, j, temporaria;

	//Laços para trabalharem com o vetor e ordená-los.
	for(i = 0; i < n; ++i)
	{
		for(j = i + 1; j < n; ++j)
		{
			//Tomada de decisão para ver se o valor de i é maior que j.	
			if(vetor[i] > vetor [j])
			{
				//Invertendo os valores de i e j, para colocá-los em ordem crescente
				temporaria = vetor [i];
				vetor [i] = vetor [j];
				vetor [j] = temporaria;
			}
		}
	}
}