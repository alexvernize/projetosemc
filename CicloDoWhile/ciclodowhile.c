//Exemplo de ciclo dowhile em C.

#include <stdio.h>

int main(){
	
	int i = 0;
	/*No ciclo while é vista a condição, se ela atender os 
	 * requisitos entra no ciclo, senão ela só ignora as linhas*/

	while (i!=0){
	
		printf("Teste\n");
		++i;
	
	/*No ciclo doWhile o ciclo é executado pelo menos uma vez antes de 
	verificar a condição, mesmo que a condição não atenda 
	os requisitos nenhuma vez.*/
	}do{
		printf("Teste2\n");
	}while(i !=0);

	return 0;
}
