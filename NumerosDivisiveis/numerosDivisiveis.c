#include <stdio.h>
#include <locale.h>

int main(){
	
	setlocale(LC_ALL, "Portuguese");
	int num1, num2;

	printf("Digite o primeiro numero: ");
	scanf("%i", &num1);
	printf("Digite o segundo numero: ");
	scanf("%i", &num2);

	if(num2 == 0){
		printf("\nDivis�o por 0 n�o � permitida.\n\n");
	}else{
		if(num1 % num2 == 0){
			printf("\n%i � divis�vel por %i.\n\n", num1, num2);
		}else
			printf("\n%i n�o � divis�vel por %i.\n\n", num1, num2);
}

	return 0;
}
