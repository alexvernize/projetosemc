//Introdução a matrizes na linguagem de programação C.

#include <stdio.h>

int main()
{
	/*Para criar uma matriz na linguagem C, usamos a ideia de vetor, com o 
	 *colchetes para os valore, mas nesse exemplo abaixo criamos uma variável 
	 *com dois espaços de colchetes. o primeiro bloco de colchetes são as linhas
	 *da matriz e o segundo são as colunas da matriz. Nesse exemplo usamos uma 
	 *matriz bidimensional, mas pode ser criadas uma tridimencional. 
	 *Uma das formas de criar uma matriz é dessa forma:
	 *int matriz[5][5] = {1,2,3,4,5,6,7,8,9};
	 *Com todos os números em sequência, ela consegue reconhecer a matriz.*/

	//Essa é outra forma
	int matriz[5][5] = {{ 1,  2,  3,  4,  5},
	                    { 6,  7,  8,  9, 10},
					    {11, 12, 13, 14, 15},
					    {16, 17, 18, 19, 20},
					    {21, 22, 23, 24, 25}};

	for (int m = 0; m < 5; ++m)
	{
		for (int n = 0; n < 5; ++n)
		{
			printf("%i ", matriz[m][n]);
		}
		printf("\n");
	} 
	

	return 0;
}
