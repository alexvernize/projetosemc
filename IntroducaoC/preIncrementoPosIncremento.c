/*
 *Existe uma pequena diferença quando fazemos um pré 
 incremento e um pós incremento. Veja abaixo a diferença
 entre eles.
 * */

#include <stdio.h>

int main(void)
{
	int x = 1;
	int y = 0;
	//Quando fazemos um pré incremento a variável é primeiro
	//incrementada e depois copiada para a outra variável.
	y = ++x;
	printf("No pré incremento a variável x vale %i e a variável y vale %i\n", x, y);
	int j = 1;
	int k = 0;
	//No pós incremento primeiro a variável é copiada e depois incrementada.
	k = j++;
	printf("No pré incremento a variável x vale %i e a variável y vale %i\n", j, k);
	
	return 0;	
}
