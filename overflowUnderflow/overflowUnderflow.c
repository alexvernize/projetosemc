//Overflow ocorre quando tentamos atribuir um valor maior do que a variavel
//que estamos utilizando consegue representar. Underflow acontece quando
//tentamos atribuir um valor menor do que a variavel consegue representar.

#include <stdio.h>

int main(void)
{
	//variaveis short conseguem representar valores menores que uma variavel
	//int, por exemplo, entao quando utilizamos valores maiores do que ela
	//consegue representar, acontece um bug no codigo. O exemplo abaixo
	//mostra isso.
	short x = 2147483647;
	short y = -2147483648;

	printf("%i\n", x);
	printf("%i\n", y);

	getchar();
	return 0;
}
