//Utilização do Laço For em C

#include <stdio.h>

int main (){

	int contador;
	/*No laço for você precisa primeiramente
	inicializar o contador com um valor, nosso exemplo
       será iniciado com valor de 1, a segunda coisa a fazer 
       é criar a condição do limite do seu ciclo, enquanto ele não
      atingir esse limite ele vai continuar dentro do loop, por 
      último deve colocar o que você deseja que seu ciclo faça. 
	*/
	for (contador = 1; contador <=10; contador = contador + 1){
		/*A ação que estiver dentro das chaves 
		do ciclo será executado*/
		printf("Programando em C\n");
		printf("Ciclo for\n");

	}

	return 0;
}
