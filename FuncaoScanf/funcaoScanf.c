//Utilizando a funcao scanf

#include <stdio.h>
#include <locale.h>

int main(){

    setlocale(LC_ALL, "Portuguese");
    //criando as variaveis
    int base, altura, area;

    /*A sintaxe do scanf � a seguinte, iniciamos o printf com uma
    mensagem indicando o que queremos, ap�s isso colocamos
    a fun��o scanf.*/
    printf ("Digite o valor da base: ");
    scanf ("%i", &base);
    /*Perceba que ele pega um valor inteiro e ap�s a v�rgula adicionamos a
    vari�vel que queremos que ele pegue, note que � utilizado o & (e comercial).*/
    printf ("Digite o valor da altura: ");
    scanf ("%i", &altura);

    area = base*altura;

    printf ("A �rea do ret�ngulo �: %i\n", area);

    //Como a funcao nao retorna nada adicionamos o "return 0;"
    return 0;
}
