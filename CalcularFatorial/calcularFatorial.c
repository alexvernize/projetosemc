//Software em C para calcular o fatorial de um número 

#include <stdio.h>

int main(){
	
	int numero;
	int resposta = 1;
	int fatorial;
	
	printf("Digite um número inteiro que deseja descobrir o fatorial: ");
	scanf("%i", &numero);
	fatorial = numero;

	for(;fatorial >= 1; --fatorial){
		//resposta = resposta*fatorial é o mesmo que o abaixo 
		resposta *= fatorial;
	}
	
	printf("O fatorial de %i é : %i\n", numero, resposta);

	return 0;
}
